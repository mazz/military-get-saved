defmodule CarsShop.Militarygetsaved do
  @behaviour Crawly.Spider

  @impl Crawly.Spider
  def base_url(), do: "http://militarygetsaved.tripod.com"

  @impl Crawly.Spider
  def init() do
    # http://militarygetsaved.tripod.com/ak.html
    # http://militarygetsaved.tripod.com/findchurch.html
    [
      start_urls: [
        "http://militarygetsaved.tripod.com/findchurch.html"
      ]
    ]
  end

  # Crawly.Engine.start_spider(CarsShop.Militarygetsaved)

  @impl Crawly.Spider
  def parse_item(response) do
    urls = response.body
    |> Floki.find("a")
    |> Floki.attribute("href")

    # pagination_urls =
    #   response.body |> Floki.find(".pagination a") |> Floki.attribute("href")

    requests =
      (urls)
      |> Enum.uniq()
      |> Enum.map(&build_absolute_url/1)
      |> Enum.map(&Crawly.Utils.request_from_url/1)

    # churches = response.body |> Floki.find("span.C-18") |> Floki.text()

    # item = %{
    #   name: item_attrs |> Floki.attribute("C-18") |> List.first(),
    #   url: response.request_url
    # }

    response_list = response.body
    |> String.replace("<wbr>", "")
    |> Floki.find("span.C-18, span.C-19, span.C-20, span.C-21, span.C-22, span.C-23, span.C-24, span.C-25")
    |> Floki.text(sep: ";;")
    |> String.split("\n")

    split_fields = Enum.map(response_list, fn item -> String.split(item, ";;") end)
    no_blanks = Enum.map(split_fields, fn item -> Enum.filter(item, fn x -> x != "" end) end)

    item = %{
      churches: no_blanks,
      url: response.request_url
    }

    %Crawly.ParsedItem{:items => [item], :requests => requests}
  end

  defp build_absolute_url(url), do: URI.merge(base_url(), url) |> to_string()
end
