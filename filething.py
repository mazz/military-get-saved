#!/usr/bin/env python

import argparse
import sys
import http.client
import json
import os
import subprocess
import re
import psycopg2
import psycopg2.extras
from psycopg2.extensions import AsIs
from psycopg2 import sql
import uuid
import datetime

pastor_regex = r'\A(\w*)(\s*)(PASTOR)(\s)(\w+)(\s*)(\w*)(\s)(\w+)$'
doctrine_regex = r'(ABA|ABBCIS|ABC|ABWE|AFBM|AIBCI|AIBF|AIMF|AMBASSADOR-BC|APPALACHIAN-BC|ARLINGTON-BC|ASV|BBF|KJV|(\A)KJB|CCM|NKJV|UNAFFILIATED)'
# church_name_regex = r'(((\s)BAPTIST(\s)CHURCH($))|((\s)BAPTIST($))|((\s)TEMPLE($))|((\s)CHURCH($))|((\s)CHAPEL(?:\s|$))|((\A)IGLESIA))'
church_name_regex = r'(((\s)BAPTIST($))|((\s)TEMPLE($))|((\s)CHURCH($))|((\s)CHAPEL(?:\s|$))|((\A)IGLESIA))'
po_box_regex = r'(\s)BOX\s+'
street_regex = r'(RD|CIR|LOOP|HWY|AVE|ST|DR|LN|PK)$'
extended_city_state_zip_regex = r'(\A.*)[\s]+(\w{2})[\s]+(\d{5})(?:[-\s]\d{4})*$'
csz_captures = r'(?P<city>\A.*)[\s]+(?P<state>\w{2})[\s]+(?P<zip>(\d{5})(?:[-\s]\d{4})*)$'

home_phone_regex = r'(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?(\s+)HM(?:\s|$)'
home_phone_captures = r'(?P<home_phone>(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,}))(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?(\s+)HM(?:\s|$)'

church_phone_regex = r'(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?(\s+)CH(?:\s|$)'
church_phone_captures = r'(?P<church_phone>(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,}))(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?(\s+)CH(?:\s|$)'

cell_phone_regex = r'(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?(\s+)CELL(?:\s|$)'
cell_phone_captures = r'(?P<cell_phone>(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,}))(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?(\s+)CELL(?:\s|$)'

g_region = None
class Stream(object):
    def __init__(self):
        parser = argparse.ArgumentParser(
            description='stream a file to a streaming service',
            usage='''churches <dir>

''')
        parser.add_argument('command', help='Subcommand to run')
        # parse_args defaults to [1:] for args, but you need to
        # exclude the rest of the args too, or validation will fail
        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            print('Unrecognized command')
            parser.print_help()
            exit(1)
        # use dispatch pattern to invoke method with same name
        getattr(self, args.command)()

    def churches(self):
        parser = argparse.ArgumentParser(
            description='parse churches.json')
        # prefixing the argument with -- means it's optional
        parser.add_argument('dir')
        # parser.add_argument('livestream_url')
        # parser.add_argument('--loop', dest='loop', action='store_true')
        # parser.set_defaults(loop=False)

        # now that we're inside a subcommand, ignore the first
        # TWO argvs, ie the command (git) and the subcommand (commit)
        args = parser.parse_args(sys.argv[2:])
        print('parsing, dir: {}'.format(repr(args.dir)))
        # print('loop-through list of videos: {}'.format(repr(args.loop)))

        church_files = Stream.walk_folder(args.dir)
        print('church_files: {}'.format(repr(church_files)))

        item = 0
        # while len(church_files) > 0:
        print('len(church_files): {}'.format(repr(len(church_files))))
        
        f = church_files[0]
        # read file
        with open(f, 'r') as myfile:
            data=myfile.read()

        # parse file
        obj = json.loads(data)

        # for row in obj:
            # print("row.url: {}".format(row["url"]))
        va = obj[13]
        print('url: {}'.format(va["url"]))

        # [x.strip() for x in va["url"].split('/')]

        for place in obj:
        # for church in va["churches"]:

            last_component = place["url"].split('/')[-1]
            print('last_component: {}'.format(last_component))
            g_region = last_component.split('.')[0]
            print('g_region: {}'.format(g_region))

            for church in place["churches"]:

                visibles = self.remove_invisibles(church)
                print("visibles: {}".format(repr(visibles)))

                church_dict = self.extract_church(visibles)
                print('church_dict: {}'.format(repr(church_dict)))
                other = self.extract_other(visibles, church_dict)  
                print('other: {}'.format(repr(other)))

    # church_dict: {'pastor': ['PASTOR TIM BULLOCK'], 'doctrine': 'KJB', 'church': 'GENEVA BAPTIST CHURCH', 'po_box': 'PO BOX 425', 'zip': '68361', 'state': 'NE', 'city': 'GENEVA', 'cell_phone': '(402) 759-1759'}
    # other: ['MEETING @ 1120 F ST (SENIOR CENTER)', 'http://www.genevanebaptistchurch.org pastorbullock@windstream.net', 'SS 10, SM 11', "VISITATION, NURSING HOME MINISTRIES,LADIES' BIBLE STUDY, MEN'S PRAYER GROUP, VBS"]

                if len(church_dict.values()) > 0:
                    sourceconn = psycopg2.connect("host=localhost dbname={} user=postgres".format('churchfinder_dev'))
                    with sourceconn.cursor() as cur:
                        print('church_dict: {}'.format(repr(church_dict)))

                        pastor_list = church_dict.get('pastor', '{\"None\"}')
                        if pastor_list is list:
                            pastor_list = self.list2pgarr(pastor_list)
                        
                        church_name = church_dict.get('church_name', None)
                        print('church_name: {}'.format(repr(church_name)))

                        # trunc_church_name = None
                        # if church_name is not None:
                        #     print('len church_name: {}'.format(repr(len(church_name))))
                        #     if len(church_name) > 100:
                        #         trunc_church_name = (church_name[:98] + '..')
                        #         print('trunc church_name: {}'.format(repr(trunc_church_name)))

                        # if trunc_church_name is not None:
                        #     church_name = trunc_church_name
                        # print('assign church_name: {}'.format(repr(church_name)))

                        church_doctrine = church_dict.get('doctrine', None)
                        church_po_box = church_dict.get('po_box', None)
                        church_street_address = church_dict.get('street_address', None)
                        church_zip = church_dict.get('zip', None)
                        church_state = church_dict.get('state', None)
                        church_city = church_dict.get('city', None)
                        church_country = church_dict.get('country', None)
                        church_phone = church_dict.get('church_phone', None)
                        home_phone = church_dict.get('home_phone', None)
                        cell_phone = church_dict.get('cell_phone', None)

                        if other is list:
                            other = self.list2pgarr(other)

                        cur.execute(sql.SQL("insert into church(uuid, pastor, church_name, doctrine, po_box, street_address, zip, state, city, country, home_phone, cell_phone, church_phone, other, updated_at, inserted_at) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"), 
                        [str(uuid.uuid4()), 
                        pastor_list,
                        church_name,
                        church_doctrine,
                        church_po_box,
                        church_street_address,
                        church_zip,
                        church_state,
                        church_city,
                        church_country,
                        home_phone,
                        cell_phone,
                        church_phone,
                        other,
                        datetime.datetime.now(),
                        datetime.datetime.now()
                        ])

                        sourceconn.commit()
                

    # field :cell_phone, :string
    # field :church_name, :string
    # field :church_phone, :string
    # field :city, :string
    # field :country, :string
    # field :doctrine, :string
    # field :hash_id, :string
    # field :home_phone, :string
    # field :other, {:array, :string}
    # field :pastor, {:array, :string}
    # field :po_box, :string
    # field :state, :string
    # field :street_address, :string
    # field :uuid, Ecto.UUID
    # field :zip, :string

    def list2pgarr(self, alist):
        return '{' + ','.join(alist) + '}'

    def extract_church(self, church):
        global g_region

        states = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA", 
          "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", 
          "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", 
          "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", 
          "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY", "PR"]

        church_dict = {}
        pastors = []
        for field in church:
            print('current field: {}'.format(repr(field)))

            # remove smart quotes
            field = field.replace('“','"').replace('”','"')

            # if 'Coffee Pot Bible Fellowship' in field:
            #     field = 'Coffee Pot Bible Fellowship'

            if re.search(pastor_regex, field):
                # print("found pastor")
                pastors.append(field.strip().title())
                church_dict["pastor"] = pastors
            elif re.search(doctrine_regex, field):
                # print("found kjb")
                church_dict['doctrine'] = field.strip()
            elif re.search(church_name_regex, field):
                # print("found church")
                church_dict["church_name"] = field.strip().title()
            elif re.search(po_box_regex, field):
                # print("found po box")
                church_dict["po_box"] = field.strip()
            elif re.search(street_regex, field):
                # print("found street")
                church_dict["street_address"] = field.strip().title()
            elif re.search(extended_city_state_zip_regex, field):
                # print("found extended_city_state_zip")
                p = re.compile(csz_captures)
                regex_search = p.search(field)
                city = regex_search.group('city')
                state = regex_search.group('state')
                zip = regex_search.group('zip')
                church_dict["zip"] = zip.strip()
                church_dict["state"] = state.strip()
                church_dict["city"] = city.strip().title()

                if state in states:
                    church_dict['country'] = 'USA'
                else:
                    church_dict['country'] = g_region

            if '://' not in field and re.search(home_phone_regex, field):
                p = re.compile(home_phone_captures)
                regex_search = p.search(field)
                home_phone = regex_search.group('home_phone')
                # print("found home phone")
                church_dict["home_phone"] = home_phone.strip()
            if '://' not in field and re.search(church_phone_regex, field):
                p = re.compile(church_phone_captures)
                regex_search = p.search(field)
                church_phone = regex_search.group('church_phone')
                # print("found church phone")
                church_dict["church_phone"] = church_phone.strip()
            if '://' not in field and re.search(cell_phone_regex, field):
                p = re.compile(cell_phone_captures)
                regex_search = p.search(field)
                cell_phone = regex_search.group('cell_phone')
                # print("found cell phone")
                church_dict["cell_phone"] = cell_phone.strip()

        return church_dict

    def extract_other(self, raw_church, church_dict):
        # print('church_dict: {}'.format(church_dict))
        have_found = []

        pastors = church_dict.get('pastor')

        # unpack pastors from array
        if pastors is not None:
            for pastor in pastors:
                have_found.append(pastor)

        # add all strings to have_found to compare later
        for value in church_dict.values():
            if isinstance(value, str):
                print('value: {}'.format(repr(value)))
                have_found.append(value)
        
        other = []
        for i, raw in enumerate(raw_church):
            found_raw = False
            for j, found in enumerate(have_found):
                if found.casefold() in raw.casefold():
                    found_raw = True
                if j == (len(have_found) - 1) and found_raw == False:
                    other.append(raw)
        # print('other: {}', other)
        return other

    def remove_invisibles(self, church):
        ## elixir
        ##         no_invisibles = String.replace(String.trim(field), ~r/[\x{200B}\x{200C}\x{200D}\x{FEFF}\x{A0}]/u, "")
        # subst = re.compile(r"[\x{200B}\x{200C}\x{200D}\x{FEFF}\x{A0}]", re.IGNORECASE)

        cleaned = []
        for string in church:
            replaced = re.sub('\s+', ' ', string)
            # no_a0 = re.sub('\xa0', ' ', string)
            # print("string: {}".format(string))
            # print("replaced: {}".format(replaced))

            cleaned.append(replaced)

        return cleaned
    # def stream(self):
    #     parser = argparse.ArgumentParser(
    #         description='stream dir to live stream endpoint')
    #     # prefixing the argument with -- means it's optional
    #     parser.add_argument('dir')
    #     parser.add_argument('livestream_url')
    #     parser.add_argument('--loop', dest='loop', action='store_true')
    #     parser.set_defaults(loop=False)

    #     # now that we're inside a subcommand, ignore the first
    #     # TWO argvs, ie the command (git) and the subcommand (commit)
    #     args = parser.parse_args(sys.argv[2:])
    #     print('Running stream, dir: {}'.format(repr(args.dir)))
    #     print('loop-through list of videos: {}'.format(repr(args.loop)))

    #     video_files = Stream.walk_folder(args.dir)
    #     print('video_files: {}'.format(repr(video_files)))

    #     item = 0
    #     while len(video_files) > 0:
    #         print('len(video_files): {}'.format(repr(len(video_files))))
            
    #         f = video_files[item]
    #         # subprocess.call(['ffmpeg', '-i', f, '-f', 'mpegts', 'udp://127.0.0.1:23000'])
    #         # ffmpeg -re -i file -c copy -bsf:a aac_adtstoasc -f flv rtmp://rtmp-server.objectaaron.com/live
    #         # openvpn.publish.objectaaron.com:1935/live
    #         # ffmpeg -re -i file -c copy -bsf:a aac_adtstoasc -f flv rtmp://10.10.10.1:1935/live

    #         # ffmpeg -re -i file -c copy -bsf:a aac_adtstoasc -f flv rtmp://openvpn.publish.objectaaron.com:1935/live
    #         # brew --prefix openvpn)/sbin
    #         # sudo /usr/local/opt/openvpn/sbin/openvpn --config .keys/client.ovpn

    #         # twitch
    #         # rtmp://live.twitch.tv/app/live_253495032_1TirhAxdYQba384YcYJnCVdf5sa8rt
    #         # 

    #         subprocess.call(['ffmpeg', '-re', '-i', f, '-c', 'copy', '-bsf:a', 'aac_adtstoasc', '-f', 'flv', args.livestream_url])
    #         item = item + 1
    #         if item == len(video_files) and args.loop == True:
    #             item = 0
    #         print('item: {}', repr(item))

    @staticmethod
    def walk_folder(media_dir):
        print('walk_folder media_dir: {}'.format(repr(media_dir)))
        print('os.path.abspath(f): {}'.format(repr(os.path.abspath(media_dir))))

        all_file_paths = Stream._get_filepaths(os.path.abspath(media_dir))
        ts_paths = []
        for f in all_file_paths:
            if f.endswith('.json') or f.endswith('.csv'):
                ts_paths.append(f)
                print('output_name: {}'.format(f))
                basename = os.path.basename(f)
                print('basename: {}'.format(basename))
        return sorted(ts_paths, key=lambda i: os.path.splitext(os.path.basename(i))[0])

    def _get_filepaths(directory):
        """
        This function will generate the file names in a directory
        tree by walking the tree either top-down or bottom-up. For each
        directory in the tree rooted at directory top (including top itself),
        it yields a 3-tuple (dirpath, dirnames, filenames).
        """
        file_paths = []  # List which will store all of the full filepaths.

        # Walk the tree.
        for root, directories, files in os.walk(directory):
            for filename in files:
                # Join the two strings in order to form the full filepath.
                filepath = os.path.join(root, filename)
                file_paths.append(filepath)  # Add it to the list.

        return file_paths # Self-explanatory.

    @staticmethod
    def _get_url(base, path, headers) -> str:
        print('_get_url: {} {}'.format(repr(base), repr(headers)))
        conn = http.client.HTTPConnection(base)
        conn.connect()
        conn.request('GET', path)
        response = conn.getresponse()
        data = response.read()
        response_string = data.decode('utf-8')
        print('response_string: {}'.format(repr(response_string)))

        conn.close()
        return response_string

    @staticmethod
    def _post_url(base, path, headers, body) -> json:
        print('_post_url: {} {} {}'.format(repr(base), repr(headers), repr(body)))

        conn = http.client.HTTPConnection(base)
        conn.request("POST", path, body, headers)
        # conn.request("POST", "/request?foo=bar&foo=baz", headers)

        res = conn.getresponse()
        data = res.read()

        response_string = data.decode('utf-8')
        result = json.loads(response_string)
        print('result: {}'.format(repr(result)))
        conn.close()
        
        return result

if __name__ == '__main__':
    Stream()
